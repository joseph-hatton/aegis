import { IAuthor } from 'app/shared/model//author.model';
import { ICategory } from 'app/shared/model//category.model';

export interface IBook {
    id?: number;
    isbn?: number;
    title?: string;
    pages?: number;
    author?: IAuthor[];
    category?: ICategory[];
}

export class Book implements IBook {
    constructor(
        public id?: number,
        public isbn?: number,
        public title?: string,
        public pages?: number,
        public author?: IAuthor[],
        public category?: ICategory[]) {}
}
