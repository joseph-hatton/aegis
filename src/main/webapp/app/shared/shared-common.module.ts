import { NgModule } from '@angular/core';

import { AegisSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [AegisSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [AegisSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class AegisSharedCommonModule {}
