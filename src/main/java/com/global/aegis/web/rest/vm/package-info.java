/**
 * View Models used by Spring MVC REST controllers.
 */
package com.global.aegis.web.rest.vm;
