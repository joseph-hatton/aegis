package com.global.aegis.cucumber.stepdefs;

import com.global.aegis.AegisApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = AegisApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
