package com.global.aegis.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Joseph Hatton on 10/28/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorUnitTest {

    private static final Long ID = 1L;

    private static final String FIRSTNAME = "NAME";
    private static final String LASTNAME = "CODE";
    private static final String LOCATION = "LOCATION";

    private Author author;

    String expectedResult = "Author{" +
        "id=" + ID +
        ", firstName='" + FIRSTNAME + "'" +
        ", lastName='" + LASTNAME + "'" +
        ", location='" + LOCATION + "'" +
        '}';

    @Before
    public void setup() {

        author = new Author();
        author.setId(ID);
        author.setFirstName(FIRSTNAME);
        author.setLastName(LASTNAME);
        author.setLocation(LOCATION);
    }

    @Test
    public void equalsGetTest() {
        Long id = author.getId();
        String firstName = author.getFirstName();
        String lastName = author.getLastName();
        String location = author.getLocation();
        assertThat(id).isEqualTo(ID);
        assertThat(firstName).isEqualTo(FIRSTNAME);
        assertThat(lastName).isEqualTo(LASTNAME);
        assertThat(location).isEqualTo(LOCATION);
    }

    @Test
    public void overrideEqualsTest() {
        boolean same = author.equals(author);
        assertThat(same).isTrue();
    }

    @Test
    public void overrideNotEqualsTest() {
        boolean same = author.equals(new Author());
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsIdsTest() {
        Author author1 = new Author();
        author1.setId(1L);
        boolean same = author.equals(author1);
        assertThat(same).isTrue();
    }

    @Test
    public void overrideEqualsIdIsNullTest() {
        Author author1 = new Author();
        author1.setId(null);
        boolean same = author.equals(author1);
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsCompareIsNullTest() {
        Author author1 = new Author();
        author1.setId(ID);
        author.setId(null);
        boolean same = author.equals(author1);
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsCompareClassTest() {
        boolean same = author.equals(new Object());
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsIsNullTest() {
        boolean same = author.equals(null);
        assertThat(same).isFalse();
    }

    @Test
    public void toStringTest() {

        String actual = author.toString();
        assertThat(actual).isEqualTo(expectedResult);
    }

    @Test
    public void hashcodeTest() {

        int actual = author.hashCode();

        assertThat(actual).isEqualTo(ID.intValue());
    }


}
