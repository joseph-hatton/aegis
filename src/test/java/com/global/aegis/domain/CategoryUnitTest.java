package com.global.aegis.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Joseph Hatton on 10/28/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class CategoryUnitTest {

    private static final Long ID = 1L;

    private static final String NAME = "NAME";
    private static final String DESCRIPTION = "DESCRIPTION";

    private Category category;

    String expectedResult = "Category{" +
        "id=" + ID +
        ", name='" + NAME + "'" +
        ", description='" + DESCRIPTION + "'" +
        '}';

    @Before
    public void setup() {

        category = new Category();
        category.setId(ID);
        category.setDescription(DESCRIPTION);
        category.setName(NAME);
    }

    @Test
    public void equalsGetTest() {
        Long id = category.getId();
        String name = category.getName();
        String description = category.getDescription();
        assertThat(id).isEqualTo(ID);
        assertThat(name).isEqualTo(NAME);
        assertThat(description).isEqualTo(DESCRIPTION);
    }

    @Test
    public void overrideEqualsTest() {
        boolean same = category.equals(category);
        assertThat(same).isTrue();
    }

    @Test
    public void overrideNotEqualsTest() {
        boolean same = category.equals(new Category());
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsIdsTest() {
        Category category1 = new Category();
        category1.setId(1L);
        boolean same = category.equals(category1);
        assertThat(same).isTrue();
    }

    @Test
    public void overrideEqualsIdIsNullTest() {
        Category category1 = new Category();
        category1.setId(null);
        boolean same = category.equals(category1);
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsCompareIsNullTest() {
        Category category1 = new Category();
        category1.setId(ID);
        category.setId(null);
        boolean same = category.equals(category1);
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsCompareClassTest() {
        boolean same = category.equals(new Object());
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsIsNullTest() {
        boolean same = category.equals(null);
        assertThat(same).isFalse();
    }

    @Test
    public void toStringTest() {

        String actual = category.toString();
        assertThat(actual).isEqualTo(expectedResult);
    }

    @Test
    public void hashcodeTest() {

        int actual = category.hashCode();

        assertThat(actual).isEqualTo(ID.intValue());
    }


}
