package com.global.aegis.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by s0041852 on 7/18/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class BookUnitTest {

    private static final Long ID = 1L;

    private static final String TITLE = "title";
    private static final Long ISBN = 1L;
    private static final Integer PAGES = 100;

    private Book book;

    Set<Author> authors = new HashSet<>();

    Set<Category> categories = new HashSet<>();

    String expectedResult = "Book{" +
        "id=" + ID +
        ", isbn=" + ISBN + "" +
        ", title='" + TITLE + "'" +
        ", pages=" + PAGES + "" +
        '}';

    @Before
    public void setup() {

        book = new Book();
        book.setId(ID);
        book.setIsbn(ISBN);
        book.setPages(PAGES);
        book.setTitle(TITLE);

        Author author = new Author();
        author.setFirstName("Joseph");
        authors.add(author);
        book.setAuthors(authors);

        Category category = new Category();
        category.setName("FICTION");
        categories.add(category);
        book.setCategories(categories);

    }

    @Test
    public void equalsGetTest() {
        Long id = book.getId();
        String title = book.getTitle();
        Integer pages = book.getPages();
        Long isbn = book.getIsbn();
        Set<Author> authorSet = book.getAuthors();
        Set<Category> categorySet = book.getCategories();
        assertThat(id).isEqualTo(ID);
        assertThat(title).isEqualTo(TITLE);
        assertThat(pages).isEqualTo(PAGES);
        assertThat(isbn).isEqualTo(ISBN);

        assertThat(authorSet).isEqualTo(authors);
        assertThat(categorySet).isEqualTo(categories);
    }

    @Test
    public void overrideEqualsTest() {
        boolean same = book.equals(book);
        assertThat(same).isTrue();
    }

    @Test
    public void overrideNotEqualsTest() {
        boolean same = book.equals(new Book());
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsIdsTest() {
        Book book1 = new Book();
        book1.setId(1L);
        boolean same = book.equals(book1);
        assertThat(same).isTrue();
    }

    @Test
    public void overrideEqualsIdIsNullTest() {
        Book book1 = new Book();
        book1.setId(null);
        boolean same = book.equals(book1);
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsCompareIsNullTest() {
        Book book1 = new Book();
        book1.setId(ID);
        book.setId(null);
        boolean same = book.equals(book1);
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsCompareClassTest() {
        boolean same = book.equals(new Object());
        assertThat(same).isFalse();
    }

    @Test
    public void overrideEqualsIsNullTest() {
        boolean same = book.equals(null);
        assertThat(same).isFalse();
    }

    @Test
    public void toStringTest() {

        String actual = book.toString();
        System.out.print(actual);
        assertThat(actual).isEqualTo(expectedResult);
    }

    @Test
    public void hashcodeTest() {

        int actual = book.hashCode();

        assertThat(actual).isEqualTo(ID.intValue());
    }


}
