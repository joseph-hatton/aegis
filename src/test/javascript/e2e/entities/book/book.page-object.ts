import { element, by, ElementFinder } from 'protractor';

export class BookComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-book div table .btn-danger'));
    title = element.all(by.css('jhi-book div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class BookUpdatePage {
    pageTitle = element(by.id('jhi-book-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    isbnInput = element(by.id('field_isbn'));
    titleInput = element(by.id('field_title'));
    pagesInput = element(by.id('field_pages'));
    authorSelect = element(by.id('field_author'));
    categorySelect = element(by.id('field_category'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setIsbnInput(isbn) {
        await this.isbnInput.sendKeys(isbn);
    }

    async getIsbnInput() {
        return this.isbnInput.getAttribute('value');
    }

    async setTitleInput(title) {
        await this.titleInput.sendKeys(title);
    }

    async getTitleInput() {
        return this.titleInput.getAttribute('value');
    }

    async setPagesInput(pages) {
        await this.pagesInput.sendKeys(pages);
    }

    async getPagesInput() {
        return this.pagesInput.getAttribute('value');
    }

    async authorSelectLastOption() {
        await this.authorSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async authorSelectOption(option) {
        await this.authorSelect.sendKeys(option);
    }

    getAuthorSelect(): ElementFinder {
        return this.authorSelect;
    }

    async getAuthorSelectedOption() {
        return this.authorSelect.element(by.css('option:checked')).getText();
    }

    async categorySelectLastOption() {
        await this.categorySelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async categorySelectOption(option) {
        await this.categorySelect.sendKeys(option);
    }

    getCategorySelect(): ElementFinder {
        return this.categorySelect;
    }

    async getCategorySelectedOption() {
        return this.categorySelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class BookDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-book-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-book'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}
