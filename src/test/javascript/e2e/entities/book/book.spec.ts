/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BookComponentsPage, BookDeleteDialog, BookUpdatePage } from './book.page-object';

const expect = chai.expect;

describe('Book e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let bookUpdatePage: BookUpdatePage;
    let bookComponentsPage: BookComponentsPage;
    let bookDeleteDialog: BookDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Books', async () => {
        await navBarPage.goToEntity('book');
        bookComponentsPage = new BookComponentsPage();
        expect(await bookComponentsPage.getTitle()).to.eq('Books');
    });

    it('should load create Book page', async () => {
        await bookComponentsPage.clickOnCreateButton();
        bookUpdatePage = new BookUpdatePage();
        expect(await bookUpdatePage.getPageTitle()).to.eq('Create or edit a Book');
        await bookUpdatePage.cancel();
    });

    it('should create and save Books', async () => {
        const nbButtonsBeforeCreate = await bookComponentsPage.countDeleteButtons();

        await bookComponentsPage.clickOnCreateButton();
        await promise.all([
            bookUpdatePage.setIsbnInput('5'),
            bookUpdatePage.setTitleInput('title'),
            bookUpdatePage.setPagesInput('5'),
            bookUpdatePage.authorSelectLastOption(),
            bookUpdatePage.categorySelectLastOption()
        ]);
        expect(await bookUpdatePage.getIsbnInput()).to.eq('5');
        expect(await bookUpdatePage.getTitleInput()).to.eq('title');
        expect(await bookUpdatePage.getPagesInput()).to.eq('5');
        await bookUpdatePage.save();
        expect(await bookUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await bookComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Book', async () => {
        const nbButtonsBeforeDelete = await bookComponentsPage.countDeleteButtons();
        await bookComponentsPage.clickOnLastDeleteButton();

        bookDeleteDialog = new BookDeleteDialog();
        expect(await bookDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Book?');
        await bookDeleteDialog.clickOnConfirmButton();

        expect(await bookComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
